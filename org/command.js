// command pattern for Updates

// проект, принимающий обновления
class Project {
	constructor(){
		this.title = '';
		this.episodes = [];
	}
}
Project.updatesStack = [];

// серия (episode)
class Episode {
	constructor(title, number){
		this.title = title;
		this.number = number || 0;
	}
}

// обновление
class Update {
	constructor(project, operation){
		this.project = project;
		this.operation = operation;
	}
	execute(){
		throw new Error('Execute method is not implemented!');
	}
}

// типы операций
const OPERATIONS = {
	ChangeTitle: (project, title) => {
		// TODO: бизнес логика
		project.title = title;
	},
	AddNewEpisode: (project, episode) => project.episodes.push(episode),
}

// команды
// Обновление (комманда) названия проекта
class UpdateProjectTitle extends Update {
	constructor(project, operation, payload){
		super(project, operation);
		this.payload = payload;
	}
	execute(){
		const operation = OPERATIONS[this.operation];
		operation(this.project, this.payload);
		Project.updatesStack.push('ChangeTitle');
	}
}

// Добавление новой серии в проект
class NewEpisodeUpdate extends Update {
	constructor(project, operation) {
		super(project, operation);
	}
	execute(){
		const operation = OPERATIONS[this.operation];
		operation(this.project, new Episode('Episode', 1));
		Project.updatesStack.push('AddNewEpisode');
	}
}

// Usage
const project = new Project();
const u1 = new UpdateProjectTitle(project, 'ChangeTitle', 'Payloaded Title').execute();
const u2 = new NewEpisodeUpdate(project, 'AddNewEpisode').execute();
const u3 = new NewEpisodeUpdate(project, 'AddNewEpisode').execute();

console.log('Project ==> \t\t', project);
console.log('Updates Stack ==> \t', Project.updatesStack);
