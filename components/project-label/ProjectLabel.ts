import { Component, Prop, Vue } from 'vue-property-decorator';


@Component({
  name: 'ProjectLabel'
})
export default class ProjectLabel extends Vue {

  @Prop({
    default: () => false
  })
  public isMini: boolean;

  @Prop({
    default: () => true
  })
  public hoverable: boolean;

  @Prop({
    required: true
  })
  public img: string;

  @Prop({
    required: true
  })
  public label: string;
}
