import { Component, Prop, Vue } from 'vue-property-decorator';

import { NewsFeedItem as NewsFeedItemModel, NewsType } from '@/models/NewsFeedItem';


@Component
export default class NewsFeedItem extends Vue {

  @Prop({ required: true })
  public item: NewsFeedItemModel;

  public itemTypes = NewsType;

}
