import { Component, Prop, Vue } from 'vue-property-decorator';

import { Illustration } from '~/models/Illustration';


@Component({
  name: 'IllustrationCard'
})
export default class IllustrationCard extends Vue {

  @Prop()
  public art: Illustration;
}
