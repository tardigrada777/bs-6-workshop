import { Component, Vue } from 'vue-property-decorator';
import { namespace } from 'vuex-class';

import { CURRENT_PROJECT } from '~/store/project/getters';
import Project from '~/models/Project';

import ProjectLabel from '~/components/project-label/ProjectLabel.vue';


const ProjectModule = namespace('project');

@Component({
  components: {
    ProjectLabel
  }
})
export default class LeftSidebar extends Vue {
  public isSidebarOpened: boolean = true;

  @ProjectModule.Getter(CURRENT_PROJECT)
  public currentProject: Project | null;

  public get currentProjectId() {
    return this.currentProject?.uid;
  }
}
