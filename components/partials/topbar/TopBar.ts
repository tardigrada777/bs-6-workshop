import { Component, Vue } from 'vue-property-decorator';

import AuthService from '~/services/Auth';
import { forceRedirectTo } from '~/utils/router';

import Logo from '~/components/Logo.vue';
import TopBarMenu from '~/components/partials/topbar/components/topbar-menu/TopBarMenu.vue';
import UserDropdown from '~/components/partials/user-dropdown/UserDropdown.vue';


@Component({
  components: {
    Logo,
    TopBarMenu,
    UserDropdown
  }
})
export default class TopBar extends Vue {

  private AuthService: AuthService = new AuthService();

  public async signOut() {
    this.$nuxt.$loading.start();
    await this.AuthService.signOut();
    this.$nuxt.$loading.finish();
    forceRedirectTo('/auth');
  }
}
