import { Component, Vue } from 'vue-property-decorator';

import Project, { ProjectType } from '~/models/Project';

import ProjectLabel from '~/components/project-label/ProjectLabel.vue';


@Component({
  components: {
    ProjectLabel
  }
})
export default class TopBarMenu extends Vue {
  public isAboutDropdownOpened: boolean = false;
  public isProjectsDropdownOpened: boolean = false;

  public myProjects: Array<Project> = [];
  public myInvitations: Array<Project> = [];

  public mounted() {
    const mockProject1 = new Project({
      type: ProjectType.TEXT,
      userId: 'dsf2fewsdc',
      name: 'Название мегаэпичного проекта',
      cover: '/mock/mock_project.png',
      coverSmall: '/mock/mock_project_small.png',
      annotation: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean purus ante, luctus sed mollis' +
        ' quis, vulputate ut magna. Suspendisse arcu quam, hendrerit non enim cursus, finibus vestibulum nibh. ' +
        'Nulla bibendum lectus vel purus rhoncus interdum. Integer sed metus sed nunc convallis ullamcorper eu a ' +
        'quam. Ut viverra a diam eu imperdiet. Curabitur accumsan tempor ante, ut imperdiet odio ornare vitae. ' +
        'Donec nisi nulla, dapibus non libero eu, tincidunt consectetur erat. Maecenas molestie imperdiet nulla.' +
        ' Donec nibh arcu, porttitor ut elit quis, eleifend ornare metus. Ut mauris ipsum, tincidunt a gravida a, ' +
        'porta nec turpis. Aenean elementum libero sit amet metus congue rutrum.',
      hasText: true,
      hasAudio: true,
      hasArt: true,
      tags: [ 'НФ', 'Фэнтези', 'Философия', 'Эпик' ],
      rate: 4.5,
      uid: ''
    });
    this.myProjects = [mockProject1, mockProject1];
    this.myInvitations = [mockProject1];
  }
}
