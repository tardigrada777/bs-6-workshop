import { Component, Vue } from 'vue-property-decorator';

import BaseDropdown from '~/components/base/dropdown/BaseDropdown.vue';
import ArrowBottomIcon from 'icons/ChevronDown.vue';


@Component({
  components: {
    BaseDropdown,
    ArrowBottomIcon
  }
})
export default class WorkshopDropdownMenu extends Vue {
  public isDropdownOpened: boolean = false;
  public menuItems = [
    {
      title: 'С чего начать',
      link: '/about/get-started'
    },
    {
      title: 'Роли',
      link: '/about/roles'
    },
    {
      title: 'Обновления',
      link: '/updates'
    },
    {
      title: 'WORKFLOW',
      link: '/about/workflow'
    },
    {
      title: 'Статистика',
      link: '/my/statistics'
    }
  ]

  public onDropdownToggle(toggleState: boolean) {
    this.isDropdownOpened = toggleState;
  }
}
