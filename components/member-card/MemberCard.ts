import {Component, Prop, Vue} from 'vue-property-decorator';

import User from '~/models/User';


@Component({
  name: 'MemberCard'
})
export default class MemberCard extends Vue {
  @Prop({
    required: true
  })
  public member: User;
}
