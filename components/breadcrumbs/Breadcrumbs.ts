import { Component, Prop, Vue } from 'vue-property-decorator';
import { namespace } from 'vuex-class';

import User from '~/models/User';
import { BreadcrumbItem } from '~/models/Breadcrumbs';
import { CURRENT_USER } from '~/store/user/getters';


const UserModule = namespace('user');

@Component
export default class Breadcrumbs extends Vue {

  public breadcrumbs: Array<BreadcrumbItem> = [];

  @UserModule.Getter(CURRENT_USER)
  public currentUser: User | null;

  @Prop({
    default: () => []
  })
  public links: Array<BreadcrumbItem>;

  public mounted() {
    const firstCrumb: BreadcrumbItem = {
      name: this.currentUser?.name || 'Пользователь',
      to: '/'
    };
    this.breadcrumbs = [ firstCrumb, ...this.links ];
  }
}
