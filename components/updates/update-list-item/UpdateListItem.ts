import { Component, Prop, Vue } from 'vue-property-decorator';

import { UpdateListItem as UpdateListItemModel, UpdateType } from '~/models/Update';


@Component({
  name: 'UpdateListItem'
})
export default class UpdateListItem extends Vue {
  @Prop()
  public update: UpdateListItemModel;

  public updateTypes = UpdateType;
}
