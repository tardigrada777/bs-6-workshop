import { Component, Vue } from 'vue-property-decorator';
import { directive as onClickAway } from 'vue-clickaway';


@Component({
  directives: {
    onClickAway
  }
})
export default class BaseDropdown extends Vue {
  public isDropdownOpen: boolean = false;

  public toggle() {
    this.isDropdownOpen = !this.isDropdownOpen;
    this.$emit('toggled', this.isDropdownOpen);
  }

  public away() {
    this.isDropdownOpen = false;
    this.$emit('toggled', this.isDropdownOpen);
  }
}
