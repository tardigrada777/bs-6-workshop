import { Component, Prop, Vue } from 'vue-property-decorator';
import { Getter, Mutation, namespace } from 'vuex-class';

import ProjectService from '~/services/Project';
import Project, { ProjectType } from '~/models/Project';
import User from '~/models/User';
import { GET_ALL_PROJECTS } from '~/store/project/actions';
import { CURRENT_USER } from '~/store/user/getters';
import { LOADING, SET_LOADING } from '~/store';
import { CURRENT_PROJECT } from '~/store/project/getters';


const UserModule = namespace('user');
const ProjectModule = namespace('project');

@Component({
  name: 'NewProjectModal'
})
export default class NewProjectModal extends Vue {
  private projectService: ProjectService = new ProjectService();

  @Prop()
  public email: string;

  @Prop()
  public password: string;

  @UserModule.Getter(CURRENT_USER)
  public currentUser: User;

  public step = 0;

  @Getter(LOADING)
  public loading: boolean;

  @Mutation(SET_LOADING)
  public setLoading: (loadingState: boolean) => void;

  @ProjectModule.Action(GET_ALL_PROJECTS)
  public getAllProjects: (user: User) => void;

  public projectTypes: Array<{
    icon: string,
    name: string,
    type: ProjectType,
    selected: boolean
  }> = [
    {
      icon: '/icons/text_update.svg',
      name: 'Текстовый сериал',
      type: ProjectType.TEXT,
      selected: false
    },
    {
      icon: '/icons/music_update.svg',
      name: 'Опера',
      type: ProjectType.MUSIC,
      selected: false
    },
    {
      icon: '/icons/illustration_update.svg',
      name: 'Комикс',
      type: ProjectType.COMICS,
      selected: false
    }
  ];

  public file: Blob | null = null;

  public newProjectData: {
    type: ProjectType | null,
    name: string,
    annotation: string,
  } = {
    type: null,
    name: '',
    annotation: ''
  };

  public froalaEditorconfig = {
    events: {
      'froalaEditor.initialized': function () {
        console.log('initialized')
      }
    }
  }

  public get nextStepButtonDisabled() {
    return !this.newProjectData.type || this.step === 2;
  }

  public get prevStepButtonDisabled() {
    return this.step === 0;
  }

  public selectProjectType(projectType: {
    icon: string,
    name: string,
    type: ProjectType,
    selected: boolean
  }) {
    this.projectTypes.forEach(project => project.selected = false);
    projectType.selected = true;
    this.newProjectData.type = projectType.type;
  }

  public nextStep() {
    this.step += 1;
  }

  public prevStep() {
    this.step -= 1;
  }

  public async createProject() {
    if (!this.file) return;
    const { annotation, name, type } = this.newProjectData;
    const { uid: userId } = this.currentUser;
    const newProject = new Project({
      annotation,
      type: type || ProjectType.TEXT,
      name,
      userId,
      cover: '/mock/mock_project.png',

      // TODO: remove when image uploading will be implemented
      coverSmall: '/mock/mock_project_small.png',

      hasArt: false,
      hasAudio: false,
      hasText: false,
      rate: 0,
      tags: [],
      uid: ''
    });
    this.$nuxt.$loading.start();
    await this.projectService.createNewProject(newProject, this.file);
    this.$nuxt.$loading.finish();
    (this.$parent as any).close();
    this.setLoading(true);
    await this.getAllProjects(this.currentUser);
    this.setLoading(false);
  }
}
