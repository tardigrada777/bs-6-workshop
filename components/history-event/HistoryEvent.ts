import {Component, Prop, Vue} from 'vue-property-decorator';

import HistoryEventModel from '~/models/HistoryEvent';

import UploadIcon from '~/components/icons/events/UploadIcon.vue';


@Component({
  name: 'HistoryEvent',
  components: {
    UploadIcon
  }
})
export default class HistoryEvent extends Vue {
  @Prop()
  public historyEvent: HistoryEventModel;
}
