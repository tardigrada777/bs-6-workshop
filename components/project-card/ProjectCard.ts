import { Component, Prop, Vue } from 'vue-property-decorator';
import Project from '~/models/Project';

import ProjectLabel from '~/components/project-label/ProjectLabel.vue';


@Component({
  name: 'ProjectCard',
  components: {
    ProjectLabel
  }
})
export default class ProjectCard extends Vue {

  @Prop({
    required: true
  })
  public project: Project;
}
