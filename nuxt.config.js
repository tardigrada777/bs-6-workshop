const { resolve } = require('path');
const firebaseConfig = require('./firebase.config');

module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: 'Мастерская',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'Workshop - part of BookSerial project'
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }
    ]
  },

  router: {
    middleware: 'auth'
  },

  /*
   ** Customize the progress bar color
   */
  loading: { color: '#6C63FF', height: '4px' },

  buildModules: ['@nuxt/typescript-build'],

  modules: [
    '@nuxtjs/pwa',
    '@nuxtjs/tailwindcss',
    'nuxt-buefy',
    '@nuxtjs/firebase',
  ],

  css: [
    '~/assets/css/main.css',
    'froala-editor/css/froala_editor.pkgd.min.css',
    'froala-editor/css/froala_style.min.css'
  ],

  plugins: [
    '~plugins/LineClamp',
    '~plugins/PerfectScrollbar',
    '~plugins/VueTimeline',
    { src: '~plugins/FroalaEditor', ssr: false }
  ],

  firebase: {
    config: firebaseConfig,
    services: {
      auth: {
        persistence: 'local',
        initialize: {
          onAuthStateChangedMutation: 'user/ON_AUTH_STATE_CHANGED_MUTATION',
          onAuthStateChangedAction: null
        },
        ssr: true
      }
    },
  },

  pwa: {
    meta: false,
    icon: false,
    workbox: {
      importScripts: ['/firebase-auth-sw.js'],
      dev: true
    }
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      // if (isDev && isClient) {
      //   config.module.rules.push({
      //     enforce: 'pre',
      //     test: /\.(js|vue)$/,
      //     loader: 'eslint-loader',
      //     exclude: /(node_modules)/
      //   });
      // }
      config.resolve.alias.icons = resolve(
        process.cwd(),
        'node_modules/vue-material-design-icons'
      );
    }
  }
};
