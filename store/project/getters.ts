import { GetterTree } from 'vuex';

import { ProjectState } from '~/store/project/state';
import { RootState } from '~/store';


export const PROJECTS = 'projects';
export const CURRENT_PROJECT = 'currentProject';

export default {
  projects: ({ projects }) => projects,
  currentProject: ({ currentProject }) => currentProject
} as GetterTree<ProjectState, RootState>;
