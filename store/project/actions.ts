import { ActionTree, ActionContext } from 'vuex';

import { ProjectState } from '~/store/project/state';
import { RootState } from '~/store';
import ProjectService from '~/services/Project';
import User from '~/models/User';


export const GET_ALL_PROJECTS = 'getAllProjects';
export const GET_CURRENT_PROJECT = 'getCurrentProject';

export default {
  async getAllProjects({ commit }: ActionContext<ProjectState, RootState>, user: User) {
    const projects = await new ProjectService().getAllProjectsOfUser(user);
    commit('setProjects', projects);
  },
  async getCurrentProject({ commit }, projectID: string) {
    const project = await new ProjectService().getProjectById(projectID);
    commit('setCurrentProject', project);
  }
} as ActionTree<ProjectState, RootState>
