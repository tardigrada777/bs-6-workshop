import { MutationTree } from 'vuex';

import { ProjectState } from '~/store/project/state';


export const SET_PROJECTS = 'setProjects';
export const SET_CURRENT_PROJECT = 'setCurrentProject';

const mutations: MutationTree<ProjectState> = {
  setProjects(state: ProjectState, projects) {
    state.projects = projects;
  },
  setCurrentProject(state, project) {
    state.currentProject = project;
  }
}

export default mutations;
