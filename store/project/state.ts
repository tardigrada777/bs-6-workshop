import Project from '~/models/Project';


export class ProjectState {
  projects: Array<Project> = [];
  currentProject: Project | null = null;
}

export default () => new ProjectState();
