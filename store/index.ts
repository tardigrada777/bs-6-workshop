// FIXME: refactor or replace - init Firebase app here
import * as firebase from 'firebase/app';
import 'firebase/auth';
import firebaseConfig from '~/firebase.config';
import { Context } from '@nuxt/types';
import { ActionTree, ActionContext, GetterTree, MutationTree } from 'vuex';


if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export const LOADING = 'loading';

export const SET_LOADING = 'setLoading';

export class RootState {
  public serverAuthUser: boolean = false;
  public loading: boolean = false;
  constructor() {}
}

export const state = () => new RootState();

export const strict = false;

export const actions: ActionTree<RootState, RootState> = {
  async nuxtServerInit(
    { commit, state }: ActionContext<RootState, RootState>,
    { res }: Context
  ) {
    if (res.locals && res.locals.user) {
      state.serverAuthUser = true;
    }
  },
}

export const mutations: MutationTree<RootState> = {
  setLoading(state: RootState, loadingState) {
    state.loading = loadingState;
  }
}

export const getters: GetterTree<RootState, RootState> = {
  loading: ({ loading }: RootState) => loading
}
