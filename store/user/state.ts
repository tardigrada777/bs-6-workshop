import User from '~/models/User';


export class UserState {
  public user: User | null = null;
}

export default () => new UserState();
