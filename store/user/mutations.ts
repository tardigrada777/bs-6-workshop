import { MutationTree } from 'vuex';
import { UserInfo } from 'firebase';

import { UserState } from '~/store/user/state';
import User from '~/models/User';


export default {
  ON_AUTH_STATE_CHANGED_MUTATION: (state, { authUser }: { authUser: UserInfo | null }) => {
    if (authUser === null) return state.user = null;
    state.user = User.fromFirebase(authUser);
  }
} as MutationTree<UserState>
