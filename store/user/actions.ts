import { ActionTree } from 'vuex';
import { Context } from '@nuxt/types';

import { RootState } from '~/store';
import { UserState } from '~/store/user/state';


export default { } as ActionTree<UserState, RootState>
