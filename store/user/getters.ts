import { GetterTree } from 'vuex';

import { UserState } from '~/store/user/state';
import { RootState } from '~/store';


export const CURRENT_USER = 'currentUser';

export default {
  currentUser: ({ user }) => user,
  isUserAuthenticated: ({ user }) => user !== null,
} as GetterTree<UserState, RootState>;
