/**
 * Модель __Проекта__, должна дублировать большую часть
 * модели на бэкенде.
 */
export default class Project {
  public name: string;
  public cover: string;
  public coverSmall: string;
  public hasText: boolean;
  public hasArt: boolean;
  public hasAudio: boolean;
  public tags: string[];
  public rate: number;
  public annotation: string;
  public type: ProjectType;

  public userId: string;
  public uid: string;

  constructor(options: ProjectConstructorOptions) {
    const {
      name,
      cover,
      coverSmall,
      hasText,
      hasArt,
      hasAudio,
      tags,
      rate,
      annotation,
      type,
      userId,
      uid
    } = options;

    this.name = name;
    this.cover = cover;
    this.coverSmall = coverSmall;
    this.hasText = hasText || false;
    this.hasArt = hasArt || false;
    this.hasAudio = hasAudio || false;
    this.tags = tags || [];
    this.rate = rate;
    this.annotation = annotation;
    this.type = type;
    this.userId = userId;
    this.uid = uid;
  }
}

export enum ProjectType {
  TEXT = 'text',
  MUSIC = 'music',
  COMICS = 'comics'
}

export interface ProjectConstructorOptions {
  name: string,
  cover: string,
  coverSmall: string,
  rate: number,
  annotation: string,
  hasText?: boolean,
  hasArt?: boolean,
  hasAudio?: boolean,
  tags?: string[],
  type: ProjectType,
  userId: string,
  uid: string
}
