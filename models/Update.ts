export class UpdateListItem {
  public type: UpdateType;
  public commentsCount: number;
  public createdBy: string;

  //  FIXME: replace with actual model of Episode
  public episode: string;

  public constructor(options: UpdateListItemConstructorOptions) {
    const {
      type,
      episode,
      createdBy,
      commentsCount
    } = options;

    this.type = type;
    this.episode = episode;
    this.createdBy = createdBy;
    this.commentsCount = commentsCount || 0;
  }

}

export interface UpdateListItemConstructorOptions {
  type: UpdateType;
  episode: string;
  createdBy: string;
  commentsCount?: number;
}

export enum UpdateType {
  TEXT = 'text',
  MUSIC = 'music',
  ILLUSTRATION = 'illustration'
}
