/**
 * Модель ссылки передаваемой в компонент __Breadcrumbs__.
 */
export interface BreadcrumbItem {
  name: string;
  to: string
}
