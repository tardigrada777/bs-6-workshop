export enum NewsType {
  FINANCE = 'finance',
  SUCCESS = 'success',
  COMMENT = 'comment',
  UPDATES = 'updates'
}

export class NewsFeedItem {
  type: NewsType;
  date: string;
  name: string;
  // FIXME: replace with Episode, Serial etc.
  target: string;

  public constructor(options: NewsFeedItemConstructorOptions) {
    const {
      type,
      date,
      name
    } = options;

    this.type = type || NewsType.SUCCESS;
    this.date = date;
    this.name = name;
    this.target = 'Новая серия'
  }

  public static finance(
    options: NewsFeedItemConstructorOptions
  ): NewsFeedItem {
    return new NewsFeedItem({
      ...options,
      type: NewsType.FINANCE
    });
  }

  public static success(
    options: NewsFeedItemConstructorOptions
  ): NewsFeedItem {
    return new NewsFeedItem({
      ...options,
      type: NewsType.SUCCESS
    });
  }

  public static comment(
    options: NewsFeedItemConstructorOptions
  ): NewsFeedItem {
    return new NewsFeedItem({
      ...options,
      type: NewsType.COMMENT
    });
  }

  public static updates(
    options: NewsFeedItemConstructorOptions
  ): NewsFeedItem {
    return new NewsFeedItem({
      ...options,
      type: NewsType.UPDATES
    });
  }
}

interface NewsFeedItemConstructorOptions {
  type?: NewsType,
  date: string,
  name: string,
}
