import { UserInfo } from 'firebase';

/**
 * Модель __User__. Должна дублировать модель на сервере.
 */
export default class User {
  public uid: string;
  public name: string;
  public avatar: string;
  public role: UserRole;

  constructor(options: UserConstructorOptions) {
    const {
      avatar,
      name,
      role,
      uid
    } = options;

    this.uid = uid;
    this.avatar = avatar;
    this.name = name;
    this.role = role || UserRole.WRITER;
  }

  public static fromFirebase({ uid, displayName }: UserInfo): User {
    return new User({
      uid,
      avatar: '',
      name: displayName || 'Default Name'
    });
  }
}

interface UserConstructorOptions {
  name: string,
  avatar: string,
  role?: UserRole,
  uid: string
}

export enum UserRole {
  WRITER = 'Писатель',
  DESIGNER = 'Художник',
  COMPOSER = 'Композитор'
}
