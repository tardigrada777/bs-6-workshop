import Project from '~/models/Project';

/**
 * Модель __HistoryEvent__ - действие пользователя
 * сохраненное в базе.
 */
export default class HistoryEvent {
  constructor(
    public type: HistoryEventType,
    public description: string,
    public project: Project
  ) {}
}

export enum HistoryEventType {
  UPLOAD= 'upload'
}
