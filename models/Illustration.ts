export class Illustration {
  public id: number;
  public name: string;
  public commentsCount: number;
  public likesCount: number;

  //  FIXME: replace with actual model of Episode
  public episode: string;

  public src: string;

  public constructor(options: IllustrationConstructorOptions) {
    const {
      id,
      name,
      episode,
      commentsCount,
      likesCount,
      src
    } = options;

    this.id = id;
    this.name = name;
    this.episode = episode;
    this.commentsCount = commentsCount || 0;
    this.likesCount = likesCount || 0;
    this.src = src;
  }
}

export interface IllustrationConstructorOptions {
  id: number;
  name: string;
  episode: string;
  src: string;
  commentsCount?: number;
  likesCount?: number;
}
