import { Component, Vue } from 'vue-property-decorator';
import { namespace } from 'vuex-class';

import Project from '~/models/Project';
import { BreadcrumbItem } from '~/models/Breadcrumbs';
import { CURRENT_PROJECT } from '~/store/project/getters';
import { first } from '~/utils/arrays';

import Breadcrumbs from '~/components/breadcrumbs/Breadcrumbs.vue';


const ProjectModule = namespace('project');

@Component( {
  layout: 'project',
  name: 'ProjectPage',
  middleware: 'project',
  components: {
    Breadcrumbs
  }
})
export default class ProjectPage extends Vue {

  // public project: Project | null;

  @ProjectModule.Getter(CURRENT_PROJECT)
  public project: Project | null;

  public links: Array<BreadcrumbItem> = [
    {
      name: 'Название проекта',
      to: '/project/12'
    }
  ];

  public created() {
    if (this.project) {
      const firstLink = first<BreadcrumbItem>(this.links);
      firstLink.name = this.project.name;
    }
    // this.project = new Project({
    //   type: ProjectType.TEXT, userId: '23sfgf67suyvgzchx3432rdcf32EC',
    //   name: 'Название мегаэпичного проекта',
    //   cover: '/mock/mock_project.png',
    //   coverSmall: '/mock/mock_project_small.png',
    //   annotation: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean purus ante, luctus sed mollis' +
    //     ' quis, vulputate ut magna. Suspendisse arcu quam, hendrerit non enim cursus, finibus vestibulum nibh. ' +
    //     'Nulla bibendum lectus vel purus rhoncus interdum. Integer sed metus sed nunc convallis ullamcorper eu a ' +
    //     'quam. Ut viverra a diam eu imperdiet. Curabitur accumsan tempor ante, ut imperdiet odio ornare vitae. ' +
    //     'Donec nisi nulla, dapibus non libero eu, tincidunt consectetur erat. Maecenas molestie imperdiet nulla.' +
    //     ' Donec nibh arcu, porttitor ut elit quis, eleifend ornare metus. Ut mauris ipsum, tincidunt a gravida a, ' +
    //     'porta nec turpis. Aenean elementum libero sit amet metus congue rutrum.',
    //   hasText: true,
    //   hasAudio: true,
    //   hasArt: true,
    //   tags: [ 'НФ', 'Фэнтези', 'Философия', 'Эпик' ],
    //   rate: 4.5,
    //   uid: ''
    // });
  }
}
