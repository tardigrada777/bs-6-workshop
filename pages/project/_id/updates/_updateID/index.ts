import { Component, Vue } from 'vue-property-decorator';
import { namespace } from 'vuex-class';
import { PerfectScrollbar } from 'vue2-perfect-scrollbar';

import { BreadcrumbItem } from '~/models/Breadcrumbs';
import { CURRENT_PROJECT } from '~/store/project/getters';
import Project from '~/models/Project';
import { first } from '~/utils/arrays';

import Breadcrumbs from '~/components/breadcrumbs/Breadcrumbs.vue';


const ProjectModule = namespace('project');

@Component({
  layout: 'project',
  components: {
    Breadcrumbs,
    PerfectScrollbar
  }
})
export default class ConcreteUpdate extends Vue {
  @ProjectModule.Getter(CURRENT_PROJECT)
  public project: Project | null;

  public links: Array<BreadcrumbItem> = [
    {
      name: 'Название проекта',
      to: '/project/12'
    },
    {
      name: 'Обновления',
      to: '/project/12/updates'
    },
    {
      name: 'Обновление №234521',
      to: '/project/12/updates/234521'
    }
  ];

  public created() {
    if (this.project) {
      const firstLink = first<BreadcrumbItem>(this.links);
      firstLink.name = this.project.name;
    }
  }
}
