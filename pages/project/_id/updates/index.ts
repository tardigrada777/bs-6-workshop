import { Component, Vue } from 'vue-property-decorator';
import { namespace } from 'vuex-class';

import { BreadcrumbItem } from '~/models/Breadcrumbs';
import { UpdateListItem as UpdateListItemModel, UpdateType } from '~/models/Update';

import { CURRENT_PROJECT } from '~/store/project/getters';
import Project from '~/models/Project';
import { first } from '~/utils/arrays';

import Breadcrumbs from '~/components/breadcrumbs/Breadcrumbs.vue';
import UpdateListItem from '~/components/updates/update-list-item/UpdateListItem.vue';


const ProjectModule = namespace('project');

@Component({
  name: 'UpdatesPage',
  layout: 'project',
  components: {
    Breadcrumbs,
    UpdateListItem
  }
})
export default class UpdatesPage extends Vue {
  @ProjectModule.Getter(CURRENT_PROJECT)
  public project: Project | null;

  public links: Array<BreadcrumbItem> = [
    {
      name: 'Название проекта',
      to: '/project/12'
    },
    {
      name: 'Обновления',
      to: '/project/12/updates'
    }
  ];

  public updates: Array<UpdateListItemModel> = [
    new UpdateListItemModel({
      commentsCount: 12,
      createdBy: 'Ilon Musk',
      episode: 'Серия 1',
      type: UpdateType.ILLUSTRATION
    }),
    new UpdateListItemModel({
      commentsCount: 12,
      createdBy: 'Каролина Стругацкая',
      episode: 'Серия 2',
      type: UpdateType.TEXT
    }),
    new UpdateListItemModel({
      commentsCount: 12,
      createdBy: 'Ilon Musk 333',
      episode: 'Серия 2',
      type: UpdateType.MUSIC
    })
  ];

  public created() {
    if (this.project) {
      const firstLink = first<BreadcrumbItem>(this.links);
      firstLink.name = this.project.name;
    }
  }
}
