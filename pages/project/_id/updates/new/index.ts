import { Component, Vue } from 'vue-property-decorator';
import { namespace } from 'vuex-class';

import { BreadcrumbItem } from '~/models/Breadcrumbs';
import { first } from '~/utils/arrays';

import { CURRENT_PROJECT } from '~/store/project/getters';
import Project from '~/models/Project';

import Breadcrumbs from '~/components/breadcrumbs/Breadcrumbs.vue';


const ProjectModule = namespace('project');

@Component({
  layout: 'project',
  components: {
    Breadcrumbs
  }
})
export default class NewUpdate extends Vue {
  @ProjectModule.Getter(CURRENT_PROJECT)
  public project: Project | null;

  public links: Array<BreadcrumbItem> = [
    {
      name: 'Название проекта',
      to: '/project/12'
    },
    {
      name: 'Обновления',
      to: '/project/12/updates'
    },
    {
      name: 'Новое обновление',
      to: ''
    }
  ];

  public froalaModel = 'Init text';
  public froalaEditorconfig = {
    events: {
      'froalaEditor.initialized': function () {
        console.log('initialized')
      }
    }
  }

  public created() {
    if (this.project) {
      const firstLink = first<BreadcrumbItem>(this.links);
      firstLink.name = this.project.name;
    }
  }
}
