import { Component, Vue } from 'vue-property-decorator';
import { namespace } from 'vuex-class';

import { BreadcrumbItem } from '~/models/Breadcrumbs';
import { CURRENT_PROJECT } from '~/store/project/getters';
import Project from '~/models/Project';
import { first } from '~/utils/arrays';

import Breadcrumbs from '~/components/breadcrumbs/Breadcrumbs.vue';


const ProjectModule = namespace('project');

@Component({
  name: 'ReviewsPage',
  layout: 'project',
  components: {
    Breadcrumbs
  }
})
export default class ReviewsPage extends Vue {
  @ProjectModule.Getter(CURRENT_PROJECT)
  public project: Project | null;

  public links: Array<BreadcrumbItem> = [
    {
      name: 'Название проекта',
      to: '/project/12'
    },
    {
      name: 'Отзывы',
      to: ''
    }
  ];

  public reviews = [
    { number: 1523, episode: 'True Detective', pubDate: '03/12/11', author: 'Hater', rate: 2.0, reviewId: '5sdf32dfsff5sdf' },
    { number: 1523, episode: 'True Detective', pubDate: '03/12/11', author: 'Hater', rate: 2.0, reviewId: '5sdfsf5s32fddf' },
    { number: 1523, episode: 'True Detective', pubDate: '03/12/11', author: 'Hater', rate: 2.0, reviewId: '5sdf32dfsff5sdf' },
  ];

  public getLabelColor(value: number) {
    return {
      'is-success': value >= 4,
      'is-warning': value < 4 && value > 3,
      'is-danger': value <= 3
    }
  }

  public readReview(reviewId: string) {
    console.log(`Reading review with ID=${reviewId}`);
  }

  public created() {
    if (this.project) {
      const firstLink = first<BreadcrumbItem>(this.links);
      firstLink.name = this.project.name;
    }
  }
}
