import { Component, Vue } from 'vue-property-decorator';
import { namespace } from 'vuex-class';

import { BreadcrumbItem } from '~/models/Breadcrumbs';
import { Illustration } from '~/models/Illustration';
import { CURRENT_PROJECT } from '~/store/project/getters';
import Project from '~/models/Project';
import { first } from '~/utils/arrays';

import Breadcrumbs from '~/components/breadcrumbs/Breadcrumbs.vue';
import IllustrationCard from '~/components/illustration-card/IllustrationCard.vue';


const ProjectModule = namespace('project');

@Component({
  layout: 'project',
  components: {
    Breadcrumbs,
    IllustrationCard
  }
})
export default class ArtsPage extends Vue{
  @ProjectModule.Getter(CURRENT_PROJECT)
  public project: Project | null;

  public links: Array<BreadcrumbItem> = [
    {
      name: 'Название проекта',
      to: '/project/12'
    },
    {
      name: 'Иллюстрации',
      to: '/project/12/members'
    }
  ];

  public arts: Array<Illustration> = [
    new Illustration({
      commentsCount: 0,
      episode: 'Серия 1',
      id: 0,
      likesCount: 23,
      name: 'Прикольное имя иллюстрации',
      src: '/mock/mock_illustration_1.png'
    }),
    new Illustration({
      commentsCount: 0,
      episode: 'Серия 3',
      id: 1,
      likesCount: 21,
      name: 'Прикольное имя иллюстрации 2',
      src: '/mock/mock_illustration_2.png'
    }),
  ];

  public created() {
    if (this.project) {
      const firstLink = first<BreadcrumbItem>(this.links);
      firstLink.name = this.project.name;
    }
  }
}
