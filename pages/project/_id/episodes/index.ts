import { Component, Vue } from 'vue-property-decorator';
import { namespace } from 'vuex-class';

import { BreadcrumbItem } from '~/models/Breadcrumbs';
import { CURRENT_PROJECT } from '~/store/project/getters';
import Project from '~/models/Project';
import { first } from '~/utils/arrays';

import Breadcrumbs from '~/components/breadcrumbs/Breadcrumbs.vue';


const ProjectModule = namespace('project');

@Component({
  name: 'EpisodesPage',
  layout: 'project',
  components: {
    Breadcrumbs
  }
})
export default class EpisodesPage extends Vue {
  @ProjectModule.Getter(CURRENT_PROJECT)
  public project: Project | null;

  public links: Array<BreadcrumbItem> = [
    {
      name: 'Название проекта',
      to: '/project/12'
    },
    {
      name: 'Серии',
      to: '/project/12/updates'
    }
  ];

  public seasonOneData = [
    { number: 1, episodeTitle: 'True Detective', pubDate: '03/12/11', views: 1325, awe: 4.0, rating: 3.9 },
    { number: 2, episodeTitle: 'True Detective 2', pubDate: '11/12/11', views: 565, awe: 3.6, rating: 4.0 },
    { number: 3, episodeTitle: 'True Detective 3', pubDate: '19/12/11', views: 123, awe: 4.3, rating: 2.9 }
  ];

  public getLabelColor(value: number) {
    return {
      'is-success': value >= 4,
      'is-warning': value < 4 && value > 3,
      'is-danger': value <= 3
    }
  }

  public created() {
    if (this.project) {
      const firstLink = first<BreadcrumbItem>(this.links);
      firstLink.name = this.project.name;
    }
  }
}
