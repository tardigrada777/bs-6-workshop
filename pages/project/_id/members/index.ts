import { Component, Vue } from 'vue-property-decorator';
import { namespace } from 'vuex-class';

import { BreadcrumbItem } from '~/models/Breadcrumbs';
import User, { UserRole } from '~/models/User';
import { CURRENT_PROJECT } from '~/store/project/getters';
import Project from '~/models/Project';
import { first } from '~/utils/arrays';

import Breadcrumbs from '~/components/breadcrumbs/Breadcrumbs.vue';
import MemberCard from '~/components/member-card/MemberCard.vue';


const ProjectModule = namespace('project');

@Component({
  layout: 'project',
  name: 'MembersPage',
  components: {
    Breadcrumbs,
    MemberCard
  }
})
export default class MembersPage extends Vue {
  @ProjectModule.Getter(CURRENT_PROJECT)
  public project: Project | null;

  public links: Array<BreadcrumbItem> = [
    {
      name: 'Название проекта',
      to: '/project/12'
    },
    {
      name: 'Участники',
      to: '/project/12/members'
    }
  ];

  public members: Array<User> = [
    new User({
      name: 'Илон Маск',
      avatar: '/mock/mock_user1.png',
      uid: '0'
    }),
    new User({
      name: 'Каролина Стругацкая',
      role: UserRole.COMPOSER,
      avatar: '/mock/mock_user2.png',
      uid: '1'
    })
  ];

  public created() {
    if (this.project) {
      const firstLink = first<BreadcrumbItem>(this.links);
      firstLink.name = this.project.name;
    }
  }
}
