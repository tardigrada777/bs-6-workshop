import { Component, Vue } from 'vue-property-decorator';
import { namespace } from 'vuex-class';

import { BreadcrumbItem } from '~/models/Breadcrumbs';
import { NewsFeedItem as NewsFeedItemModel } from '~/models/NewsFeedItem';
import { CURRENT_PROJECT } from '~/store/project/getters';
import Project from '~/models/Project';
import { first } from '~/utils/arrays';

import Breadcrumbs from '~/components/breadcrumbs/Breadcrumbs.vue';
import NewsFeedItem from '~/components/news-feed-item/NewsFeedItem.vue';


const ProjectModule = namespace('project');

@Component({
  layout: 'project',
  components: {
    Breadcrumbs,
    NewsFeedItem
  }
})
export default class NewsPage extends Vue {
  @ProjectModule.Getter(CURRENT_PROJECT)
  public project: Project | null;

  public links: Array<BreadcrumbItem> = [
    {
      name: 'Название проекта',
      to: '/project/12'
    },
    {
      name: 'Новости',
      to: '/project/12/members'
    }
  ];

  public newsItems: Array<NewsFeedItemModel> = [
    NewsFeedItemModel.finance({
      name: 'Ежедневные продажи сериала',
      date: '31.10.2019, 07:15'
    }),
    NewsFeedItemModel.finance({
      name: 'Ежедневные продажи сериала',
      date: '31.10.2019, 07:15'
    }),
    NewsFeedItemModel.success({
      name: 'Приняты обновления текста',
      date: '31.10.2019, 07:15'
    }),
    NewsFeedItemModel.comment({
      name: 'Новый отзыв',
      date: '31.10.2019, 07:15'
    }),
    NewsFeedItemModel.updates({
      name: 'Предложено обновление',
      date: '31.10.2019, 07:15'
    })
  ];

  public created() {
    if (this.project) {
      const firstLink = first<BreadcrumbItem>(this.links);
      firstLink.name = this.project.name;
    }
  }
}
