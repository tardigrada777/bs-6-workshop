import { Component, Vue, Watch } from 'vue-property-decorator';
import { namespace } from 'vuex-class';
import * as firebase from 'firebase/app';
import Error = firebase.auth.Error;
import UserCredential = firebase.auth.UserCredential;
import AuthError = firebase.auth.AuthError;

import AuthService from '~/services/Auth';
import User from '~/models/User';
import { forceRedirectTo } from '~/utils/router';

import Logo from '~/components/Logo.vue';


const UserModule = namespace('user');

@Component({
  name: 'SignInPage',
  layout: 'empty',
  components: {
    Logo
  }
})
export default class AuthPage extends Vue {

  private AuthService: AuthService = new AuthService();

  @UserModule.Getter('currentUser')
  public currentUser: User | null;

  public availableStates = AuthPageState;
  public pageState: AuthPageState = AuthPageState.SIGNIN;

  public loading: boolean = false;

  public signUpData: { email: string, pass: string } = {
    email: '',
    pass: ''
  };

  public signInData: { email: string, pass: string } = {
    email: '',
    pass: ''
  };

  public hasEmailError: boolean = false;
  public emailErrorMessage: string = '';

  public hasPasswordError: boolean = false;
  public passErrorMessage: string = '';

  public get canSignUp() {
    return this.signUpData.email.length != 0 && this.signUpData.pass.length >= 6;
  }

  public get canSignIn() {
    return this.signInData.email.length != 0 && this.signInData.pass.length >= 6;
  }

  async signUp() {
    this.loading = true;
    this.$nuxt.$loading.start();

    const res = await this.AuthService.signUpWithEmailAndPassword(
      this.signUpData.email,
      this.signUpData.pass
    );
    // FIXME: improve Error detecting
    if ('code' in res && res.code) {
      this.loading = false;

      // @ts-ignore
      this.$nuxt.$loading.fail();

      if (
        res.code === 'auth/email-already-in-use' ||
        res.code === 'auth/invalid-email'
      ) {
        this.hasEmailError = true;
        this.emailErrorMessage = res.message;
      }
      if (res.code === 'auth/weak-password') {
        this.hasPasswordError = true;
        this.passErrorMessage = res.message;
      }
      this.$nuxt.$loading.finish();
    }
    else {
      this.loading = false;
      this.$nuxt.$loading.finish();
      forceRedirectTo('/');
    }
  }

  async signIn() {
    this.loading = true;
    this.$nuxt.$loading.start();

    const res = await this.AuthService.signInWithEmailAndPassword(
      this.signInData.email,
      this.signInData.pass
    );
    // FIXME: improve Error detecting
    if ('code' in res && res.code) {
      this.loading = false;

      // TODO: why $nuxt.$loading is possibly undefined?
      // @ts-ignore
      this.$nuxt.$loading.fail();

      if (
        res.code === 'auth/invalid-email' ||
        res.code === 'auth/user-not-found' ||
        res.code === 'auth/too-many-requests'
      ) {
        this.hasEmailError = true;
        this.emailErrorMessage = res.message;
      }
      if (res.code === 'auth/wrong-password') {
        this.hasPasswordError = true;
        this.passErrorMessage = res.message;
      }
      this.$nuxt.$loading.finish();
    }
    else {
      this.loading = false;
      this.$nuxt.$loading.finish();
      forceRedirectTo('/');
    }
  }

  @Watch('signInData.email', { deep: true })
  public emailWatcher() {
    this.hasEmailError = false;
    this.emailErrorMessage = '';
  }

  @Watch('signInData.pass', { deep: true })
  public passwordWatcher() {
    this.hasPasswordError = false;
    this.passErrorMessage = '';
  }

  @Watch('signUpData.email', { deep: true })
  public signupEmailWatcher() {
    this.hasEmailError = false;
    this.emailErrorMessage = '';
  }

  @Watch('signUpData.pass', { deep: true })
  public signupPasswordWatcher() {
    this.hasPasswordError = false;
    this.passErrorMessage = '';
  }
}

export enum AuthPageState {
  SIGNIN,
  SIGNUP
}
