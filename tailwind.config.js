/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  theme: {
    extend: {
      colors: {
        'bsw-purple': '#BC9CFF',
        'bsw-green': '#6FCF97',
        'bsw-accent': '#F64C56',
        'bsw-gray-5': 'rgba(31, 32, 65, 0.05)',
        'bsw-gray-50': 'rgba(31, 32, 65, 0.5)'
      },
      borderRadius: {
        'large': '30px'
      }
    }
  },
  variants: {},
  plugins: [],
  purge: {
    // Learn more on https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css
    enabled: process.env.NODE_ENV === 'production',
    content: [
      'components/**/*.vue',
      'layouts/**/*.vue',
      'pages/**/*.vue',
      'plugins/**/*.js',
      'nuxt.config.js'
    ]
  }
};
