import firebase from 'firebase/app';
import 'firebase/auth';
import Error = firebase.auth.Error;
import UserCredential = firebase.auth.UserCredential;
import AuthError = firebase.auth.AuthError;

export default class AuthService {
  private db: firebase.auth.Auth;

  constructor() {
    this.db = firebase.auth();
  }

  public async signUpWithEmailAndPassword(
    email: string,
    password: string
  ): Promise<UserCredential | AuthError> {
    try {
      return await this.db.createUserWithEmailAndPassword(email, password);
    }
    catch (ex) {
      console.log('service', ex);
      return ex;
    }
  }

  public async signInWithEmailAndPassword(
    email: string,
    password: string
  ): Promise<UserCredential | Error> {
    try {
      return await this.db.signInWithEmailAndPassword(email, password);
    }
    catch (ex) {
      return ex;
    }
  }

  public async signOut() {
    await this.db.signOut();
  }

  public get currentUser() {
    return this.db.currentUser;
  }
}
