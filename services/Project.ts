import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/storage';

import { v4 as uuidv4 } from 'uuid';

import Project from '~/models/Project';
import User from '~/models/User';
import CyrillicToTranslit from 'cyrillic-to-translit-js';
import { compressImage } from '~/utils/images';

export default class ProjectService {
  private db: firebase.firestore.Firestore;
  private projectsCollection: firebase.firestore.CollectionReference;
  private storage: firebase.storage.Storage;
  private coverRef: firebase.storage.Reference;
  private minCoverRef: firebase.storage.Reference;

  private ctt: CyrillicToTranslit;

  private coverPath: string = 'project/cover/';
  private minCoverPath: string = 'project/cover/min/';

  constructor() {
    this.db = firebase.firestore();
    this.storage = firebase.storage();
    this.projectsCollection = this.db.collection('projects');
    this.ctt = new CyrillicToTranslit();
  }

  public async createNewProject(project: Project, cover: Blob) {
    if (!cover) return;
    const translitName = this.ctt.transform(project.name, '_').toLowerCase();
    const { coverUrl, minCoverUrl } = await this.uploadCover(cover, translitName);
    project.cover = coverUrl;
    project.coverSmall = minCoverUrl;
    await this.projectsCollection.add({ ...project });
  }

  public async getAllProjectsOfUser(user: User): Promise<Array<Project>> {
    const projects: Array<Project> = [];
    const docSnapshots = await this.projectsCollection.where('userId', '==', user.uid).get();
    docSnapshots.forEach(doc => projects.push(new Project({ ...doc.data() as any, uid: doc.id })));
    return projects;
  }

  public async getProjectById(projectID: string): Promise<Project> {
    const docSnapshot = await this.projectsCollection.doc(projectID).get();
    return new Project({ ...docSnapshot.data() as any, uid: docSnapshot.id });
  }

  public async uploadCover(file: Blob, projectName: string): Promise<{ coverUrl: string, minCoverUrl: string }> {
    const [ _, ext ] = file.type.split('/');
    const coverName = `${projectName}-${uuidv4()}.${ext}`;
    this.coverRef = this.storage.ref(`${this.coverPath}${coverName}`);
    await this.coverRef.put(file);
    const coverUrl = await this.coverRef.getDownloadURL();
    const minCoverUrl = await this.uploadMinimizedCover(file, coverName);
    return { coverUrl, minCoverUrl };
  }

  public async uploadMinimizedCover(file: Blob, coverName: string): Promise<string> {
    const minimizedCover = await compressImage(file, coverName);
    this.minCoverRef = this.storage.ref(`${this.minCoverPath}min__${coverName}`)
    await this.minCoverRef.put(minimizedCover);
    return await this.minCoverRef.getDownloadURL();
  }
}
