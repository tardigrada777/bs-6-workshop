import { Middleware } from '@nuxt/types';
import { GET_CURRENT_PROJECT } from '~/store/project/actions';

const getCurrentProjectMiddleware: Middleware = async ({ store, route }) => {
  await store.dispatch(`project/${GET_CURRENT_PROJECT}`, route.params.id);
};

export default getCurrentProjectMiddleware;
