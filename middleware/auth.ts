import { Middleware } from '@nuxt/types';

const authenticatedMiddleware: Middleware = ({ store, redirect, res}) => {
  if (!store.state.serverAuthUser) {
    return redirect('/auth')
  }
  if (res && !res.locals.user) {
    return redirect('/auth');
  }
};

export default authenticatedMiddleware;
