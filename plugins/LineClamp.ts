import Vue from 'vue';
import LineClamp from './vue-line-clamp.node';

if (process.client) {
  Vue.use(LineClamp);
}
