import Vue from 'vue';
import PerfectScrollbarPlugin from 'vue2-perfect-scrollbar';
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css';

Vue.use(PerfectScrollbarPlugin);
