// require('froala-editor/js/froala_editor.pkgd.min.js');

import Vue from 'vue';
import VueFroala from 'vue-froala-wysiwyg';
import 'froala-editor/js/plugins/align.min.js'
import 'froala-editor/js/froala_editor.min.js';

if (process.client) {
  Vue.use(VueFroala);
}
