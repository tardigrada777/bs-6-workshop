export const compressImage = (
  imageFile: Blob,
  fileName: string,
  widths?: number,
  height?: number
): Promise<File> => {
  return new Promise<File>((resolve, reject) => {
    const WIDTH = widths || 100;
    const HEIGHT = height || 100;
    const reader = new FileReader();
    reader.readAsDataURL(imageFile);
    reader.onload = event => {
      if (!event.target) return reject('Minimizing Error');
      const img = new Image();
      img.src = event.target.result as string;
      img.onload = () => {
        const canvas = document.createElement('canvas');
        canvas.width = WIDTH;
        canvas.height = HEIGHT;
        const ctx = canvas.getContext('2d');
        if (!ctx) return reject('Minimizing Error')
        ctx.drawImage(img, 0, 0, WIDTH, HEIGHT);
        ctx.canvas.toBlob(async (blob) => {
          const minFile = new File([blob as Blob], `min__${fileName}`, {
            type: imageFile.type,
            lastModified: Date.now()
          });
          resolve(minFile);
        }, imageFile.type, 1);
      }
      reader.onerror = error => reject(`Minimizing Error: ${error}`);
    };
  });
};
