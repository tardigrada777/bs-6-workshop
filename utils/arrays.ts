export const first = <T>(array: T[]): T => array[0];
