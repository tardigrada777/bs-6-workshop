export const forceRedirectTo = (to: string) => window.location.href = to;
